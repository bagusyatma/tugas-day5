import java.util.ArrayList;

public class App {
    public static void main(String[] args){
//        membuat arraylist 1
        ArrayList listOne = new ArrayList<>();
        listOne.add("aku");
        listOne.add("kamu");
        listOne.add("dia");

        System.out.println("List 1 = " + listOne);

//        membuat arraylist 2
        ArrayList<String> listTwo = new ArrayList<>();
        listTwo.add("dia");
        listTwo.add("mereka");

        System.out.println("List 2 = " + listTwo);

        System.out.println("==========");

        ArrayList<String> newList = new ArrayList<>();
        newList.addAll(listOne);

//        System.out.println("newList = " + newList);

        newList.retainAll(listTwo);

//        System.out.println("newList = " + newList);

        listOne.addAll(listTwo);

//        System.out.println("List 1 = " + listOne);

        listOne.removeAll(newList);

        System.out.println("List 1 = " + listOne);

        System.out.println("==========");

        for (int i = 0; i < listOne.size(); i++){
            System.out.println("list = " +listOne.get(i));
        }
    }
}

//  menambahkan semua isi list 1 ke newList
//  retainAll newList dengan list 2 (menghapus yg tidak sama, yg sama [dia])
//  menambahkan isi list 1 dengan isi list 2 menjadi [aku, kamu, dia, dia, mereka]
//  mengahpus isi list 1 yg sama dengan isi newList